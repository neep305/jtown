## Automating workflow
``` bash
$ npm install -g nodemon

$ npm start

> jtown@1.0.0 start /Users/jason/dev/project/jtown
> nodemon app/app.js

[nodemon] 1.11.0
[nodemon] to restart at any time, enter `rs`
[nodemon] watching: *.*
[nodemon] starting `node app/app.js`
Listening on port 3000

$ npm install -g reload

$ npm install --save reload <-- you can see dependencies in package.json
```
