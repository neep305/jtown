#!/bin/bash

# any future command that fails will exit the script
set -e

# Using Ubuntu
# curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install -y npm
sudo npm install pm2@latest -g
# Delete the old repo
rm -rf /home/ubuntu/jtown/

# clone the repo again
git clone https://gitlab.com/neep305/jtown.git

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
# source /home/ubuntu/.nvm/nvm.sh
# source $PATH

# stop the previous pm2
sudo pm2 kill
sudo npm remove pm2 -g

#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
sudo npm install pm2 -g
# starting pm2 daemon
sudo pm2 status

cd /home/ubuntu/jtown

rm -rf .env
echo "PORT=80" > .env

#install npm packages
echo "Running npm install"
npm install

#Restart the node server
echo "Running pm2"
sudo pm2 start npm --name "jtown" -- run start