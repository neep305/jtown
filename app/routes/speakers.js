var express = require('express');
var router = express.Router();

router.get('/speakers', function(req,res){
  //get data from JSON file
  var data = req.app.get('appData');
  var pagePhotos = [];
  var pageSpeakers = data.speakers;

  data.speakers.forEach(function(item){
    pagePhotos = pagePhotos.concat(item.artwork);
  });

  res.render('speakers', {
    pageTitle: 'Speakers',
    artwork: pagePhotos,
    speakers: pageSpeakers,
    pageID: 'speakerList'
  });
  // var info = '';
  // var dataFile = req.app.get('appData');  //add this for imporing dataFile
  //
  // dataFile.speakers.forEach(function(item) {
  //   info += `
  //   <li>
  //     <h2>${item.name}</h2>
  //     <img src="/images/speakers/${item.shortname}_tn.jpg" alt="speaker" style="height: 300px;">
  //
  //     <p>${item.summary}</p>
  //   </li>
  //   `;
  // });
  //
  // res.send(`
  //   <link rel="stylesheet" type="text/css" href="css/style.css">
  //   <h1>Roux Academy Meetups</h1>
  //   ${info}
  // `);
});

router.get('/speakers/:speakerid', function(req,res){
  var data = req.app.get('appData');
  var pagePhotos = [];
  var pageSpeakers = [];

  data.speakers.forEach(function(item){
    if (item.shortname == req.params.speakerid) {
      pageSpeakers.push(item);
      pagePhotos = pagePhotos.concat(item.artwork);
    }
  });

  res.render('speakers', {
    pageTitle: 'Speakers Info',
    artwork: pagePhotos,
    speakers: pageSpeakers,
    pageID: 'speakerDetail'
  });
  // var dataFile = req.app.get('appData');  //add this for imporing dataFile
  // var speaker = dataFile.speakers[req.params.speakerid];
  //
  // res.send(`
  //   <link rel="stylesheet" type="text/css" href="/css/style.css">
  //   <h1>Roux Academy Meetups</h1>
  //   <h1>${speaker.title}</h1>
  //   <h2>with ${speaker.name}</h2>
  //   <img src="/images/speakers/${speaker.shortname}_tn.jpg" alt="speaker" style="height: 300px;">
  //   <p>${speaker.summary}</p>
  // `);
});

module.exports = router;
