var express = require('express');
var router = express.Router();

/*
author: Jason Nam

- to get your ip : Typing what is your ip in Google Search then you can get a real ip
*/
router.get('/map', function(req,res){
  res.send(`
    <div id="map" style="width:500px;height:400px;"></div>
    <script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=684970dd5a243a9bb80133269427efe8"></script>
    <script>
    var container = document.getElementById('map'); //지도를 담을 영역의 DOM 레퍼런스
    var options = { //지도를 생성할 때 필요한 기본 옵션
    	center: new daum.maps.LatLng(33.450701, 126.570667), //지도의 중심좌표.
    	level: 3 //지도의 레벨(확대, 축소 정도)
    };

    var map = new daum.maps.Map(container, options); //지도 생성 및 객체 리턴
    </script>
  `);
});

module.exports = router;
