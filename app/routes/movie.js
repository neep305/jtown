var express = require('express');
var router = express.Router();

router.get('/movie', function (req,res) { 
    res.render('movie',{
        pageTitle: 'Movie Main',
        pageID: 'movie'
    });
});

module.exports = router;