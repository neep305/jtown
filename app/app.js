var express = require('express');
var reload = require('reload');
var app = express();
var dataFile = require('./data/data.json');
var io = require('socket.io')();

require('dotenv').config();

app.set('port', process.env.PORT || 3000);

//add this code to pass the data.json to other js class(index.js & speakers.js)
app.set('appData', dataFile);

//set ejs
app.set('view engine', 'ejs');

app.set('views','app/views');

//title for using in index.ejs like <%= siteTitle %>
app.locals.siteTitle = 'Jason Meetups';
app.locals.allSpeakers = dataFile.speakers;

//add this code to load all static files. (Don't need write exact extract filename)
app.use(express.static('app/public'));
app.use(require('./routes/index'));
app.use(require('./routes/speakers'));
app.use(require('./routes/map'));
app.use(require('./routes/feedback'));
app.use(require('./routes/api'));
app.use(require('./routes/chat'));
app.use(require('./routes/movie'));

var server = app.listen(app.get('port'), function(){
  console.log('Listening on port ' + app.get('port'));
});

io.attach(server);

io.on('connection', function(socket){
  console.log('User Connected');

  socket.on('postMessage', function(data){
    io.emit('updateMessages', data);
  });
});

reload(server, app);

// var http = require('http');
//
// var myServer = http.createServer(function(req,res){
//   res.writeHead(200, {"Content-Type":"text/html"});
//   res.write('<h1>Roux Meetups</h1>');
//   res.end();
// });
//
// myServer.listen(3000);
// console.log('Go to localhost:3000 on your browser');
